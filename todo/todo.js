﻿var todoModel = (function () {

    var _data = [];

    function _addItem(name, description, completed) {
        _data.push({
            id: getCurrentId(),
            name: name,
            description: description,
            completed: completed
        });
    }


    function _changeItem(id) {
        _data.forEach(function (e, index) {
            if (e.id == id) {
                _data[index].completed = e.completed ? 0 : 1;
            }
        })
    }

    function _removeItem(id) {
        _data.forEach(function (e, index) {
            if (e.id == id) {
                _data.splice(index, 1);
            }
        })
    }

    function _save() {
        window.localStorage["tasks"] = JSON.stringify(_data, function (key, val) {
            if (key == '$$hashKey') {
                return undefined;
            }
            return val
        });
    }

    function _read() {
        var temp = window.localStorage["tasks"]

        if (!temp) _data = [];
        else _data = JSON.parse(temp);

        return _data;
    }

    function getCurrentId() {
        if (_data.length == 0) return 0;
        else return _data[_data.length - 1].id+1;
    }

    return {
        data: _data,
        addItem: _addItem,
        changeItem: _changeItem,
        removeItem: _removeItem,
        save: _save,
        read: _read
    };

})();