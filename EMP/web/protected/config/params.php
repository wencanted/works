<?php

// this contains the application parameters that can be maintained via GUI
return array(
	// this is displayed in the header section
	'title'=>'',
	// this is used in error pages
	'adminEmail'=>'webmaster@example.com',
);
