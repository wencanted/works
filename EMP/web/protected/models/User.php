<?php

/**
 * This is the model class for table "{{users}}".
 *
 * The followings are the available columns in table '{{users}}':
 * @property integer $id
 * @property string $email
 * @property string $password
 * @property string $name
 * @property integer $role
 * @property integer $active
 */
class User extends CActiveRecord
{
	public $verifyCode;
	public $password2;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{users}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, password, name', 'required', 'message' => '{attribute} не может быть пустым'),
			array('password2, verifyCode', 'required', 'message' => '{attribute} не может быть пустым', 'on' => 'register, ajax'),
			array('email', 'length', 'max'=>50),
			array('password, name', 'length', 'max'=>30),
			array('email', 'email', 'message' => 'Неверный формат {attribute}'),
			array('email', 'unique', 'message' => '{attribute} "{value}" уже зарегистрирован'),
			array('id, email, password, name, role, active', 'safe', 'on'=>'search'),
			array('password2', 'passwordsMatch', 'message' => 'Пароли не совпадают', 'on' => 'register, ajax'),
			array('verifyCode', 'captcha', 'allowEmpty'=>!CCaptcha::checkRequirements(), 'message' => 'Код введен не правильно', 'on' => 'register'),
		);
	}

	public function passwordsMatch($attribute,$params) {
		if($this->$attribute != $this->password)
			$this->addError($attribute, $params['message']);
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'email' => 'Email',
			'password' => 'Пароль',
			'password2' => 'Повтор пароля',
			'name' => 'Имя',
			'role' => 'Роль',
			'active' => 'Аккаунд подтвержден',
			'verifyCode' => 'Код проверки',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('role',$this->role);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
