<?php 
$this->pageTitle=Yii::app()->name . ' - Регистрация';
?>

<div class="container form-reg">
	<h1>Регистрация</h1>
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'reg-form',
		'enableAjaxValidation'=>true,
		'clientOptions' => array(
        	'validateOnSubmit' => true,
        	'validateOnChange' => true,        
    	),
	)); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',['class' => 'form-control']); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',['class' => 'form-control']); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'password2'); ?>
		<?php echo $form->passwordField($model,'password2',['class' => 'form-control']); ?>
		<?php echo $form->error($model,'password2'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',['class' => 'form-control']); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model,'verifyCode'); ?>
		<br><?$this->widget('CCaptcha')?>
		<?php echo $form->textField($model,'verifyCode',['class' => 'form-control']); ?>
		<?php echo $form->error($model,'verifyCode'); ?>
	</div>

	<div class="submit">
		<?php echo CHtml::submitButton('Login',['class' => 'btn btn-default']); ?>
	</div>

<?php $this->endWidget(); ?>
</div>