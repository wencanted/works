<?php

/**
 * SiteController is the default controller to handle user requests.
 */
class SiteController extends CController
{
    public function actions(){
        return array(
            'captcha'=>array(
                'class'=>'CCaptchaAction',
            ),
        );
    }
	/**
	 * Index action is the default action in a controller.
	 */
	public function actionIndex() {
		$this->render('index');
	}

	public function actionConfirm() {
		$this->render('confirm');
	}

	public function actionEmailConfirm() {

		$id = $_GET['id'];
		$code = $_GET['code'];

		$model = User::model()->findByPk($id);

		if($model && $code == md5($model->email.'p'.$model->password)) {
			$model->active = 1;
			if($model->save()) {
				$this->redirect(['/site/index']);
			} else {
				throw new CHttpException("", "Ошибка активации");
			}
		}
		else {
			throw new CHttpException("", "Ошибка активации");
		}
	}

	public function actionRegister() {

		$model = new User();
		$model->setScenario('register');

		if(isset($_POST['ajax']) && $_POST['ajax']==='reg-form')
		{
			$model->setScenario('ajax');
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];

			if($model->validate()) {

				if($model->save()) {
					$this->sendEmail($model);
					$this->redirect(['/site/confirm']);
				}
	
			}
		}

		$this->render('register', ['model' => $model]);
	}

	public function sendEmail($model) {
		$subject = 'Регистрация на сайте '.Yii::app()->name;
		$url = 'http://domen.ru/index.php/site/emailConfirm?id='.$model->id.'&code='.md5($model->email.'p'.$model->password);
		$body = 'Для того, чтобы активировать учетную запись перейдите по ссылке '.$url;
		$headers= "MIME-Version: 1.0\r\n";
		$headers .= "Content-type: text/html; charset=utf-8\r\n";
		$headers .= "From: Yii::app()->name  <mail@domen.ru>\r\n";
		$headers .= "Reply-To: mail@domen.ru\r\n";
		mail($model->email, $subject,$body, $headers);
	}
}