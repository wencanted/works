<?
namespace App;
/**
 * Класс для создания объекта аналога memcached
 * 
 * @author Shevtsov Dmitriy <dima90-dima@mail.ru>
 */
class Cache {

	private function __construct() {}

	/**
	 * Создает объект для работы с аналогом memcached
	 *
	 * @param integer $type: тип создаваемого хранилища. 1 - на файлах, любое другое значение или пустое - на mysql.
	 * @return object: объект аналога memcached
	 */
	static public function get($type = null) {

		if($type == 1) {
			return new Cache\CacheFile();
		} else {
			return new Cache\CacheSql();
		}

	}
}