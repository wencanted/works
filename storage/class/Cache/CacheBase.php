<?
namespace App\Cache;
/**
 * Абстрактый класс, описывающий основные функции для хранения данных с стиле memcached
 * 
 * @author Shevtsov Dmitriy <dima90-dima@mail.ru>
 */
abstract class CacheBase {

	/**
	 * Свойство класса
	 *
	 * @var integer: время жизни записи
	 */
	protected $timelife = 3600;

	abstract public function get($key);
	abstract public function set($key, $value);
	abstract public function delete($key);
	abstract public function flush();
}