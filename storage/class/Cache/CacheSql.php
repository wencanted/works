<?
namespace App\Cache;
/**
 * Класс для работы с аналогом memcached на MySQL
 * 
 * @author Shevtsov Dmitriy <dima90-dima@mail.ru>
 */
class CacheSql extends CacheBase {

	/**
	 * Возвращает ранее сохраненную запись по ключу 
	 *
	 * @param integer $key: ключ
	 * @return all: ранее сохраненное значение, любого типа, кроме resourse.
	 */
	public function get($key) {

		if(!is_int($key)) {
			throw new \Exception("The key can only be an integer", 1);
		}

		$db = \App\DB::getDB();
		$smtp = $db->prepare("SELECT 1, value, time FROM cache WHERE id_key=:key");
		$smtp->execute(['key'=>$key]);
		list($isCache, $value, $cacheTime) = $smtp->fetch(\PDO::FETCH_NUM);

		if($isCache) {
			$cacheTime = strtotime($cacheTime);
			if((time() - $cacheTime) >= $this->timelife) {
				$this->delete($key);
				return false;
			}
			return unserialize($value);
		}

		return false;
	}

	/**
	 * Сохраняет запись по ключу 
	 *
	 * @param integer $key: ключ
	 * @param all $value: данные, любого типа кроме resourse
	 * @return bool: 1 - в случае успешного сохранения, 0 - в случае ошибки
	 */
	public function set($key, $value) {

		if(!is_int($key)) {
			throw new \Exception("The key can only be an integer", 1);
		}
			
		if(is_resource($value)) {
			throw new \Exception("The value cant be a resource", 1);
		}

		$data = serialize($value);

		$db = \App\DB::getDB();
		$smtp = $db->prepare("SELECT 1 FROM cache WHERE id_key=:key");
		$smtp->execute(['key'=>$key]);
		$isCache = $smtp->fetchColumn();

		if($isCache) {
			$db->prepare("UPDATE cache SET value = :value, time = NOW() WHERE id_key = :key")->execute(['value'=>$data,'key'=>$key]);
		} else {
			$db->prepare("INSERT INTO cache SET id_key = :key ,value = :value, time = NOW()")->execute(['value'=>$data,'key'=>$key]);
		}
	}

	/**
	 * Удаляет запись по ключу 
	 *
	 * @param integer $key: ключ
	 */
	public function delete($key) {

		if(!is_int($key)) {
			throw new \Exception("The key can only be an integer", 1);
		}

		$db = \App\DB::getDB();
		$db->prepare("DELETE FROM cache WHERE id_key = :key")->execute(['key'=>$key]);

	}

	/**
	 * Удаляет все устаревшие записи
	 *
	 */
	public function flush() {
		$db = \App\DB::getDB();
		$db->prepare("DELETE FROM cache WHERE time < (NOW() - interval ? second)")->execute([$this->timelife]);
	}
}
 