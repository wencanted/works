<?
namespace App\Cache;
/**
 * Класс для работы с аналогом memcached на файлах
 * 
 * @author Shevtsov Dmitriy <dima90-dima@mail.ru>
 */
class CacheFile extends CacheBase {

	/**
	 * Свойство класса
	 *
	 * @var string: имя папки, в которой хранятся записи
	 */
	protected $cacheDir = 'cacheFile';

	/**
	 * Свойство класса
	 *
	 * @var string: имя папки, в которой хранятся записи в md5
	 */
	protected $cacheDirMD5;

	/**
	 * Конструктор. Создает папку для хранения записей, если ее не сущесвует
	 *
	 */
	public function __construct() {

		$this->cacheDirMD5 = md5($this->cacheDir)."\\";

		if(!file_exists($this->cacheDirMD5)) {
			if(!mkdir($this->cacheDirMD5, '0777')) {
				throw new Exception("Failed to create folder", 1);			
			}
		}

	}

	/**
	 * Возвращает ранее сохраненную запись по ключу 
	 *
	 * @param integer $key: ключ
	 * @return all: ранее сохраненное значение, любого типа, кроме resourse.
	 */
	public function get($key) {

		if(!is_int($key)) {
			throw new \Exception("The key can only be an integer", 1);
		}

		$cache = glob($this->cacheDirMD5."[0-9]*-".$key);
		if(!$cache) {
			return false;
		}

		$cache = $cache[0];

		if($this->isCacheOutdated($cache)) {
			$this->delete($key);
			return false;
		}

		$file = fopen($cache, "r");
		$data = fread($file, filesize($cache));
		fclose($file);

		$data = unserialize($data);

		return $data;

	}

	/**
	 * Сохраняет запись по ключу 
	 *
	 * @param integer $key: ключ
	 * @param all $value: данные, любого типа кроме resourse
	 * @return bool: 1 - в случае успешного сохранения, 0 - в случае ошибки
	 */
	public function set($key, $value) {


		if(!is_int($key)) {
			throw new \Exception("The key can only be an integer", 1);
		}
			
		if(is_resource($value)) {
			throw new \Exception("The value cant be a resource", 1);
		}

		$this->delete($key);
			
		$time = time();
		$file = fopen($this->cacheDirMD5.$time."-".$key, "w");
		
		if(!fwrite($file, serialize($value))) {
			return 0;
		}

		fclose($file);

		return 1;
	}

	/**
	 * Удаляет запись по ключу 
	 *
	 * @param integer $key: ключ
	 */
	public function delete($key) {

		if(!is_int($key)) {
			throw new \Exception("The key can only be an integer", 1);
		}

		$oldCache = glob($this->cacheDirMD5."[0-9]*-".$key);
		if(count($oldCache)>0) {
			foreach ($oldCache as $oldCacheFile) {
				unlink($oldCacheFile);
			}
		}
	}

	/**
	 * Удаляет все устаревшие записи
	 *
	 */
	public function flush() {

		$cacheFiles = scandir($this->cacheDirMD5);
		foreach ($cacheFiles as $cacheFile) {

			if ($cacheFile == "." || $cacheFile == "..") {
				continue;
			}

			if($this->isCacheOutdated($cacheFile)) {
				unlink($this->cacheDirMD5.$cacheFile);
			} else {
				return;
			}
		}
	}

	/**
	 * Проверяет, устарела ли запись
	 *
	 * @param string $filename: имя файла в котором хранится значение
	 * @return bool: 1 - запись устарела,  0 - запись не устарела. 
	 */
	protected function isCacheOutdated($filename) {

		$file = explode("-", $filename);
		$timeCreation = str_replace($this->cacheDirMD5, "", $file[0]);
		if((time() - $timeCreation) >= $this->timelife) {
			return 1;
		}

		return 0;
	}
}
 