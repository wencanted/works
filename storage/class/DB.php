<?
namespace App;
/**
 * Класс для доступа к базе данных
 * 
 * @author Shevtsov Dmitriy <dima90-dima@mail.ru>
 */
class DB {
	/**
	 * Свойство класса
	 *
	 * @var resourse: сохраняет подключение к базе данных
	 */
	private static $link;

	private function __construct() {}

	/**
	 * Подключение к базе данных
	 *
	 */
	static function dbConnect()
	{
		$db_data = \App::$config['db'];
		$dsn = "mysql:host=".$db_data['server'].";dbname=".$db_data['name'].";charset=utf8";
		self::$link = new \PDO($dsn, $db_data['user'], $db_data['pass']); 
	}

	/**
	 * Возвращает открытое соединение с базой данных или создает новое
	 *
	 */
	static function getDB()
	{
		if(!self::$link) {
			self::dbConnect();
		}

		return self::$link;
	}


}