<?
//namespace App;
/**
 * Класс запуска приложения
 * 
 * @author Shevtsov Dmitriy <dima90-dima@mail.ru>
 */
class App  {

	/**
	 * Свойство класса
	 *
	 * @var array: массив с настройками
	 */
	public static $config;

	/**
	 * Сохраняет настройки
	 *
	 * @param array $config: массив с настройками
	 */
	public static function run($config) {
		self::$config = $config;
	}

	/**
	 * Автозагрузка классов
	 *
	 * @param string $className: имя загружаемого класса.
	 */
	public static function autoload($className) {

		$classPath = explode("\\", $className);
	    array_shift($classPath);

	    $classFile = dirname(__FILE__);

	   	foreach ($classPath as $value) {
	   		$classFile .= '\\'.$value;
	   	}

	   	$classFile .= '.php';

	   	if(file_exists($classFile)) {
	  		include_once($classFile);
	   	}
	}
}

spl_autoload_register(['App','autoload'], true, true);