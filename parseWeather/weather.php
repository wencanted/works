<?
include_once("class/Weather.php");


$w = new Weather();
$wToday = $w->getToday();
$w10Days = $w->get10Days();

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8"/>
	<title>Погода в Москве</title>
	<style type="text/css">
		tr.h {
			text-align: center;
			font-weight: bold;
		}
		tr.b {
			text-align: center;
			height: 50px;
		}
		tr.b > td {
			width: 200px;
		}
	</style>
</head>
<body>
	<h2>Погода на сегодня:</h2>
	<table>
		<tr class="h">
			<td>Утром</td>
			<td>Днем</td>
			<td>Вечером</td>
			<td>Ночью</td>
		</tr>
		<tr class="b">
			<td>
				<?=$wToday['morning']->tMin?> .. <?=$wToday['morning']->tMax?>
				<br><?=$wToday['morning']->condition?>		
			</td>
			<td>
				<?=$wToday['day']->tMin?> .. <?=$wToday['day']->tMax?>
				<br><?=$wToday['day']->condition?>	
			</td>
			<td>
				<?=$wToday['evening']->tMin?> .. <?=$wToday['evening']->tMax?>
				<br><?=$wToday['evening']->condition?>	
			</td>
			<td>
				<?=$wToday['night']->tMin?> .. <?=$wToday['night']->tMax?>
				<br><?=$wToday['night']->condition?>	
			</td>
		</tr>
	</table>

	<h2>Погода на 10 дней:</h2>
	<table>
		<tr class="h">
			<td>Дата</td>
			<td>Утром</td>
			<td>Днем</td>
			<td>Вечером</td>
			<td>Ночью</td>
		</tr>
		<? foreach ($w10Days as $wDay) { ?>
		<tr class="b">
			<td>
				<?=date("d.m.Y",strtotime($wDay['date']))?>		
			</td>
			<td>
				<?=$wDay['morning']->tMin?> .. <?=$wDay['morning']->tMax?>
				<br><?=$wDay['morning']->condition?>		
			</td>
			<td>
				<?=$wDay['day']->tMin?> .. <?=$wDay['day']->tMax?>
				<br><?=$wDay['day']->condition?>	
			</td>
			<td>
				<?=$wDay['evening']->tMin?> .. <?=$wDay['evening']->tMax?>
				<br><?=$wDay['evening']->condition?>	
			</td>
			<td>
				<?=$wDay['night']->tMin?> .. <?=$wDay['night']->tMax?>
				<br><?=$wDay['night']->condition?>	
			</td>
		</tr>
		<? } ?>
	</table>
</body>
</html>
