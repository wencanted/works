<?
/**
 * Класс для доступа к базе данных
 * 
 * @author Shevtsov Dmitriy <dima90-dima@mail.ru>
 */
class DB {
	/**
	 * Хост для подключения к БД
	 */
	const host = "localhost";

	/**
	 * Имя базы данных
	 */
	const dbname = "cache";

	/**
	 * Имя пользователя
	 */
	const user = "root";

	/**
	 * Пароль
	 */
	const password = "";

	/**
	 * @var resourse: сохраняет подключение к базе данных
	 */
	private static $link;

	private function __construct() {}

	/**
	 * Подключение к базе данных
	 *
	 */
	static function dbConnect()
	{
		$dsn = "mysql:host=".DB::host.";dbname=".DB::dbname.";charset=utf8";
		self::$link = new \PDO($dsn, DB::user, DB::password); 
	}

	/**
	 * Возвращает открытое соединение с базой данных или создает новое
	 *	@return  resourse: ресурс для работы с БД
	 */
	static function getDB()
	{
		if(!self::$link) {
			self::dbConnect();
		}

		return self::$link;
	}


}