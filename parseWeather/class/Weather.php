<?
include_once("DB.php");

/**
 * Класс для работы с данными о погоде
 * @author Shevtsov Dmitriy <dima90-dima@mail.ru>
 */
class Weather
{
	
	/**
	 * Сохраняем погоду в БД
	 * @param array: массив с данными о погоде на 10 дней
	 */
	public function set($weatherWeek) {

		$db = DB::getDB();

		foreach ($weatherWeek as $weatherDay) {
			$smtp = $db->prepare("SELECT 1 FROM weather WHERE date=:date");
			$smtp->execute(['date'=>$weatherDay['date']]);
			$isDay = $smtp->fetchColumn();

			$morning = json_encode($weatherDay['morning']);
			$day = json_encode($weatherDay['day']);
			$evening = json_encode($weatherDay['evening']);
			$night = json_encode($weatherDay['night']);

			if($isDay) {
				$db->prepare("UPDATE weather SET morning = :morning, day = :day, evening = :evening, night = :night WHERE date = :date")
				   ->execute(['morning'=>$morning,'day'=>$day,'evening'=>$evening,'night'=>$night,'date'=>$weatherDay['date']]);
			} else {
				$db->prepare("INSERT INTO weather SET date = :date, morning = :morning, day = :day, evening = :evening, night = :night")
				   ->execute(['date'=>$weatherDay['date'], 'morning'=>$morning,'day'=>$day,'evening'=>$evening,'night'=>$night]);
			}
		}

	} 

	/**
	 * Возвращает погоду на сегодня
	 * @return array: массив в данными о погоде
	 */
	public function getToday() {
		
		$db = DB::getDB();
		$date = date("Y-m-d");
		$smtp = $db->prepare("SELECT morning, day, evening, night FROM weather WHERE date=:date");
		$smtp->execute(['date'=>$date]);
		$weatherToday = $smtp->fetch(PDO::FETCH_ASSOC); 

		if($weatherToday) {
			$weatherToday['morning'] = json_decode($weatherToday['morning']);
			$weatherToday['day'] = json_decode($weatherToday['day']);
			$weatherToday['evening'] = json_decode($weatherToday['evening']);
			$weatherToday['night'] = json_decode($weatherToday['night']);
		}

		return $weatherToday;

	}

	/**
	 * Возвращает погоду на 10 дней
	 * @return array: массив в данными о погоде
	 */
	public function get10Days() {

		$db = DB::getDB();
		$weather = [];
		$smtp = $db->prepare("SELECT date, morning, day, evening, night FROM weather WHERE date >= NOW() AND date <= (NOW() + interval 10 day) ORDER BY date");
		$smtp->execute();

		while($weatherDay = $smtp->fetch(PDO::FETCH_ASSOC)) { 
			$weatherDay['morning'] = json_decode($weatherDay['morning']);
			$weatherDay['day'] = json_decode($weatherDay['day']);
			$weatherDay['evening'] = json_decode($weatherDay['evening']);
			$weatherDay['night'] = json_decode($weatherDay['night']);
			$weather[] = $weatherDay;
		}

		return $weather;

	}
}