<?
include_once("phpQuery.php");

/**
 * Класс для парсинга погоды
 * 
 * @author Shevtsov Dmitriy <dima90-dima@mail.ru>
 */
class WeatherParse {
	/**
	 * Ссылка по которой будем парсить погоду
	 */
	const weatherUrl = "https://yandex.ru/pogoda/moscow/details";

	/**
	 * Получаем html код страницы с погодой
	 * @return string: html код
	 */
	public function getWeatherHTML() {
		$ch = curl_init(WeatherParse::weatherUrl);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$html = curl_exec($ch);
		curl_close($ch);
		return $html;
	}

	/**
	 * Обрабатываем html код
	 * @return array: массив с погодой, вида [0] => ['date' => date, 'morning' => ['tMin' => 'минимальная t', tMax => 'максимальная t', 'contidion' => 'облачно'], day => [...], ... ] ...;
	 */
	public function run() {

		$html = $this->getWeatherHTML();
		$pq = phpQuery::newDocumentHTML($html);
		$weather = [];
		$date = date("Y-m-d");

		foreach ($pq->find("tbody.weather-table__body") as $day) {
		  $vs = $day->firstChild;
		  $weatherDay = [];
		  $i = 0;
		  while ($vs != false) {
		    $el = pq($vs);
		    $daypart = $el->find("div.weather-table__daypart")->elements[0]->textContent;
		    $t = explode(html_entity_decode("&hellip;"),$el->find("div.weather-table__temp")->elements[0]->textContent);
		    $condition = $el->find("td.weather-table__body-cell_type_condition > div.weather-table__value")->elements[0]->textContent;

		    if($i == 0) 
		      $daypartText = "morning";
		    elseif($i == 1)
		      $daypartText = "day";
		    elseif($i == 2)
		      $daypartText = "evening";
		    elseif($i == 3)
		      $daypartText = "night";

		    $weatherDay[$daypartText] = ['tMin' => $t[0], 'tMax' => $t[1], 'condition' => $condition];

		    $vs = $vs->nextSibling;
		    $i++;
		  }

		  $weatherDay['date'] = $date;
		  $weather[] = $weatherDay;

		  $date = date("Y-m-d",strtotime($date."+1 DAY"));

		}
		
		return $weather;
	}

}